<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id['lat']}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')


        <div class="row">
            @if($provider != 'yandex')
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" class="form-control" name="{{$name['adress']}}" placeholder="{{ old($column['adress'], $value['adress']) }}" id="search-{{$id['lat'].$id['lng']}}">
                        {{--                    <span class="input-group-btn">--}}
                        {{--                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>--}}
                        {{--                    </span>--}}
                    </div>
                </div>
            @endif
            <div style="white-space:nowrap;" class="col-md-4">

                <div style="display:inline-block;width: 40px;white-space:nowrap;">纬度:</div><input style="display:inline-block;width: 80%;" id="{{$id['lat']}}" name="{{$name['lat']}}" class="form-control" value="{{ old($column['lat'], $value['lat']) }}" {!! $attributes !!} disabled/>
            </div>
            <div style="white-space:nowrap;" class="col-md-4">
                <div style="display:inline-block;width: 40px;white-space:nowrap;">经度:</div><input style="display:inline-block;width: 80% ;" id="{{$id['lng']}}" name="{{$name['lng']}}" class="form-control" value="{{ old($column['lng'], $value['lng']) }}" {!! $attributes !!} disabled/>
            </div>

        </div>

        <br>

        <div id="map_{{$id['lat'].$id['lng']}}" style="width: 100%;height: {{ $height }}px"></div>

        @include('admin::form.help-block')

    </div>
</div>
